test:
	pytest

test_in_docker:
	sh docker-test.sh

run_in_docker:
	docker-compose -f docker-compose.yml up

run_api_server:
	make upgrade_db
	python manage.py run-api-server

upgrade_db:
	alembic -c src/core/infra/db/alembic/alembic.ini --raiseerr upgrade heads

create_migration:
	alembic -c src/core/infra/db/alembic/alembic.ini revision --autogenerate -m $(comment)

prod_requirements:
	pipenv install --system --deploy --ignore-pipfile
