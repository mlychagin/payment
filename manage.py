import click

from src.core import settings


@click.group()
def cli():
    pass


@cli.command()
def run_api_server():
    from src.core.infra.app.app_cfg import make_app
    app = make_app()
    app.go_fast(
        host='0.0.0.0',
        port=settings.API_SERVER_PORT,
    )


if __name__ == '__main__':
    cli()
