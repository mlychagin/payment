from abc import ABC


class RepoABC(ABC):
    def create(
        self, *args, **kwargs,
    ):
        ...

    async def persist(
        self, *args, **kwargs,
    ):
        ...

    async def load_by(self, *args, **kwargs):
        ...


class DomainModel(ABC):
    def __repr__(self) -> str:
        return f'{self.__class__.__name__} {vars(self)}'
