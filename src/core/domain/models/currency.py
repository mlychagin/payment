from src.core.common.interfaces import DomainModel


class Currency(DomainModel):
    def __init__(
            self,
            *,
            id=None,
            name: str,
            deleted: bool,
    ) -> None:
        super().__init__()
        self._id = id
        self._name = name
        self._deleted = deleted

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def deleted(self):
        return self._deleted
