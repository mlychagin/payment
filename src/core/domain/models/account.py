from datetime import datetime
from decimal import Decimal
from typing import Optional

from src.core.common.interfaces import DomainModel


class Account(DomainModel):
    def __init__(
            self,
            *,
            id=None,
            user_id: Optional[int] = None,
            balance: Decimal,
            currency_id,
            is_tech: bool,
            deleted=False,
            created_at: datetime=None,
    ) -> None:
        super().__init__()
        self._id = id
        self._user_id = user_id
        self._balance = balance
        self._currency_id = currency_id
        self._is_tech = is_tech
        self._deleted = deleted
        self._created_at = created_at
        self._overdraft_allowed = is_tech

    @property
    def id(self):
        return self._id

    @property
    def balance(self):
        return self._balance

    @property
    def user_id(self):
        return self._user_id

    @property
    def currency_id(self):
        return self._currency_id

    @property
    def deleted(self):
        return self._deleted

    @property
    def is_tech(self):
        """Является ли техническим аккаунтом"""
        return self._is_tech

    def set_deleted(self):
        self._deleted = True
