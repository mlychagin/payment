from decimal import Decimal
from typing import Optional

from src.core.common.interfaces import DomainModel


class Transaction(DomainModel):
    def __init__(
            self,
            *,
            withdrawal_account_id: Optional[int]=None,
            insertion_account_id: int,
            sum: Decimal,
            currency_id,
    ) -> None:
        super().__init__()

        self.withdrawal_account_id = withdrawal_account_id
        self.insertion_account_id = insertion_account_id
        self.sum = sum
        self.currency_id = currency_id
