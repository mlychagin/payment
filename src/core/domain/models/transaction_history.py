from datetime import datetime
from decimal import Decimal

from src.core.common.interfaces import DomainModel


class TransactionHistory(DomainModel):
    def __init__(
            self,
            *,
            id: int,
            created_at: datetime,
            withdrawal_account_id: int,
            insertion_account_id: int,
            sum: Decimal,
            currency_id,
            deleted: bool,
    ) -> None:
        self._id = id
        self._created_at = created_at
        self._withdrawal_account_id = withdrawal_account_id
        self._insertion_account_id = insertion_account_id
        self._sum = sum
        self._currency_id = currency_id
        self._deleted = deleted

    @property
    def id(self):
        return self._id

    @property
    def created_at(self):
        return self._created_at

    @property
    def withdrawal_account_id(self):
        return self._withdrawal_account_id

    @property
    def insertion_account_id(self):
        return self._insertion_account_id

    @property
    def sum(self):
        return self._sum

    @property
    def currency_id(self):
        return self._currency_id

    @property
    def deleted(self):
        return self._deleted
