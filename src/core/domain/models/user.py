from src.core.common.interfaces import DomainModel


class User(DomainModel):
    def __init__(
            self,
            *,
            id=None,
            first_name,
            last_name,
            middle_name,
            deleted=False,
    ) -> None:
        super().__init__()
        self._id = id
        self._first_name = first_name
        self._last_name = last_name
        self._middle_name = middle_name
        self._deleted = deleted

    @property
    def id(self):
        return self._id

    @property
    def first_name(self):
        return self._first_name

    @property
    def last_name(self):
        return self._last_name

    @property
    def middle_name(self):
        return self._middle_name

    @property
    def deleted(self):
        return self._deleted

    def set_deleted(self):
        self._deleted = True
