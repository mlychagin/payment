from sanic import Sanic

from src.core import settings
from src.core.infra.api_server.api.cfg import cfg_api_handlers
from src.core.infra.api_server.exceptions import cfg_api_exceptions
from src.core.infra.api_server.open_api_spec.open_api_spec_cfg import cfg_open_api_spec
from src.core.infra.db.orm import bind_mappers
from src.core.infra.db.persistence_provider import AiopgPersistenceProvider
from src.core.service_layer.repositories.account import AccountRepo
from src.core.service_layer.repositories.currency import CurrencyRepo
from src.core.service_layer.repositories.transaction import TransactionRepo
from src.core.service_layer.repositories.transaction_history import TransactionHistoryRepo
from src.core.service_layer.repositories.user import UserRepo


class PaymentApplication(Sanic):
    transaction_repo: TransactionRepo
    transaction_history_repo: TransactionHistoryRepo
    user_repo: UserRepo
    account_repo: AccountRepo
    currency_repo: CurrencyRepo

    def __init__(self, name=None, router=None, error_handler=None, load_env=True, request_class=None,
                 strict_slashes=False, log_config=None, configure_logging=True):
        super().__init__(name, router, error_handler, load_env, request_class, strict_slashes, log_config,
                         configure_logging)
        self._default_currency = None

    async def get_default_currency(self):
        if not self._default_currency:
            usd = await self.currency_repo.load_one_by(name='USD')
            if not usd:
                usd = self.currency_repo.create('USD')
                usd = await self.currency_repo.persist(usd)
            self._default_currency = usd
        return self._default_currency


def make_app() -> PaymentApplication:
    app = PaymentApplication(name="payment")
    persistence_provider, repos = make_db()
    for repo_name, repo in repos.items():
        setattr(app, repo_name, repo)

    cfg_api_handlers(app)
    cfg_api_exceptions(app)
    cfg_open_api_spec(app)

    @app.listener('before_server_start')
    async def pre_start(application, loop):
        await persistence_provider.connect()

    @app.listener('after_server_stop')
    async def pre_stop(application, loop):
        await persistence_provider.close()

    return app


def make_db():
    persistence_provider = AiopgPersistenceProvider(
        user=settings.PAYMENT_DB_USER,
        password=settings.PAYMENT_DB_PASSWORD,
        host=settings.PAYMENT_DB_HOST,
        port=settings.PAYMENT_DB_PORT,
        database=settings.PAYMENT_DB_NAME,
        max_conn=settings.DB_MAX_CONNECTIONS,
    )
    return persistence_provider, bind_mappers(
        persistence_provider=persistence_provider,
    )
