from functools import wraps

from sanic import response


def validate_request_json(schema):
    def do_decoration(func):
        @wraps(func)
        async def wrapper(request, *args, **kwargs):
            validated_data = schema.load(request.json)
            return await func(request, validated_data, *args, **kwargs)

        return wrapper

    return do_decoration


def not_found(msg):
    return response.json(
        body=msg,
        status=404,
    )
