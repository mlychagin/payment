def cfg_open_api_spec(app):
    from apispec import APISpec
    from apispec.ext.marshmallow import MarshmallowPlugin

    from src.core.infra.api_server.api.public.v1.accounts import CreateUserInSchema, CreateUserOutSchema
    from src.core.infra.api_server.api.public.v1.transactions import (
        DepositMoneyInSchema,
        DepositMoneyOutSchema,
        TransferMoneyInSchema,
        TransferMoneyOutSchema,
    )

    from sanic_openapi import swagger_blueprint

    app.blueprint(swagger_blueprint)
