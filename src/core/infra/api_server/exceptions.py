from marshmallow import ValidationError
from sanic import response, Sanic


async def handle_validation_error(request, exception):
    return response.json(
        exception.messages,
        status=400,
    )

async def unhandled_exception(request, exception):
    return response.json(
        str(exception),
        status=500,
    )


def cfg_api_exceptions(app: Sanic):
    app.error_handler.add(
        exception=ValidationError,
        handler=handle_validation_error,
    )
    app.error_handler.add(
        exception=Exception,
        handler=unhandled_exception,
    )
