from sanic import Sanic


def cfg_api_handlers(app: Sanic):
    from src.core.infra.api_server.api.public.v1 import public_api_v1_bp

    app.blueprint(public_api_v1_bp)
