from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from marshmallow import Schema
from marshmallow.fields import Str, Int
from sanic import response
from sanic_openapi import doc
from sanic_openapi.doc import String, Integer

from src.core.infra.api_server.utils import validate_request_json
from src.core.infra.app.app_cfg import PaymentApplication


class CreateUserInSchema(Schema):
    first_name = Str(required=True, allow_none=False)
    middle_name = Str(required=True, allow_none=False)
    last_name = Str(required=True, allow_none=False)


class CreateUserOutSchema(Schema):
    first_name = Str(required=True, allow_none=False)
    middle_name = Str(required=True, allow_none=False)
    last_name = Str(required=True, allow_none=False)
    user_id = Int(required=True, allow_none=False)
    account_id = Int(required=True, allow_none=False)


@doc.summary('Создание клиента с кошельком')
@doc.consumes(
    {
        "user info": {
            "first_name": String,
            "middle_name": String,
            "last_name": String,
        },
    },
    content_type='application/json',
    location="body",
)
@doc.response(
    201,
    {
        "first_name": String,
        "middle_name": String,
        "last_name": String,
        "user_id": Integer,
        "account_id": Integer,
    },
    description="Пользователь и аккаунт созданы",
)
@validate_request_json(CreateUserInSchema())
async def register_user_hdlr(request, validated_json):
    app: PaymentApplication = request.app
    new_user = app.user_repo.create_user(**validated_json)
    new_user = await app.user_repo.persist(new_user)
    acc = app.account_repo.create_account(
        user_id=new_user.id,
        currency_id=(await app.get_default_currency()).id,
    )
    acc = await app.account_repo.persist(acc)

    return response.json(
        CreateUserOutSchema().dump({
            "first_name": new_user.first_name,
            "middle_name": new_user.middle_name,
            "last_name": new_user.last_name,
            "user_id": new_user.id,
            "account_id": acc.id,
        }),
        status=201,
    )
