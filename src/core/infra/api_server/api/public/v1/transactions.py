from marshmallow import Schema
from marshmallow.fields import Decimal, Int, Str
from sanic import response
from sanic_openapi import doc
from sanic_openapi.doc import String, Integer

from src.core.infra.api_server.api.public.v1.accounts import CreateUserOutSchema
from src.core.infra.api_server.utils import validate_request_json, not_found
from src.core.infra.app.app_cfg import PaymentApplication
from src.core.infra.db.orm import UnspecifiedTransactionExc, WithdrawalExc


class DepositMoneyInSchema(Schema):
    account_id = Int(required=True, allow_none=False)
    sum = Decimal(required=True, allow_none=False)


class DepositMoneyOutSchema(Schema):
    account_id = Int(required=True, allow_none=False)
    user_id = Int(required=True, allow_none=False)
    balance = Decimal(required=True, allow_none=False)


@doc.summary('Зачисление денежных средств на кошелек клиента')
@doc.consumes(
    {
        "user info": {
            "account_id": Integer,
            "sum": String,
        },
    },
    content_type='application/json',
    location="body",
)
@doc.response(
    200,
    {
        "account_id": Integer,
        "user_id": Integer,
        "balance": String,
    },
    description="Средства зачислены",
)
@validate_request_json(DepositMoneyInSchema())
async def deposit_money_hdlr(request, validated_json):
    app: PaymentApplication = request.app
    account_id = validated_json['account_id']
    t = await app.transaction_repo.create_deposit(
        account_repo=app.account_repo,
        insertion_account_id=account_id,
        sum=validated_json['sum'],
        currency_id=(await app.get_default_currency()).id,
    )
    _, insertion_account, _ = await app.transaction_repo.persist(t)

    return response.json(
        DepositMoneyOutSchema().dump({
            "account_id": insertion_account.id,
            "user_id": insertion_account.user_id,
            "balance": insertion_account.balance,
        }),
        status=200,
    )


class TransferMoneyInSchema(Schema):
    withdrawal_account_id = Int(required=True, allow_none=False)
    insertion_account_id = Int(required=True, allow_none=False)
    sum = Decimal(required=True, allow_none=False)


class TransferMoneyOutSchema(Schema):
    withdrawal_account_id = Int(required=True, allow_none=False)
    insertion_account_id = Int(required=True, allow_none=False)
    sum = Decimal(required=True, allow_none=False)


@doc.summary('Перевод денежных средств с одного кошелька на другой')
@doc.consumes(
    {
        "user info": {
            "withdrawal_account_id": Integer,
            "insertion_account_id": Integer,
            "sum": String,
        },
    },
    content_type='application/json',
    location="body",
)
@doc.response(
    200,
    {
        "withdrawal_account_id": Integer,
        "insertion_account_id": Integer,
        "balance": String,
    },
    description="Средства зачислены",
)
@validate_request_json(TransferMoneyInSchema())
async def transfer_money_hdlr(request, validated_json):
    app: PaymentApplication = request.app
    t = app.transaction_repo.create_c2c(
        withdrawal_account_id=validated_json['withdrawal_account_id'],
        insertion_account_id=validated_json['insertion_account_id'],
        sum=validated_json['sum'],
        currency_id=(await app.get_default_currency()).id,
    )
    try:
        await app.transaction_repo.persist(t)
    except WithdrawalExc as e:
        return response.json(e.args[0], status=400)
    except UnspecifiedTransactionExc:
        return response.json('Transaction request not correct', status=400)

    return response.json(
        TransferMoneyOutSchema().dump({
            "withdrawal_account_id": t.withdrawal_account_id,
            "insertion_account_id": t.insertion_account_id,
            "sum": t.sum,
        }),
        status=200,
    )
