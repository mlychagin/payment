from sanic import Blueprint

from .accounts import register_user_hdlr
from .transactions import deposit_money_hdlr, transfer_money_hdlr

public_api_v1_bp = Blueprint('public_api_v1', url_prefix='/api/v1')
public_api_v1_bp.add_route(register_user_hdlr, '/user', methods=['POST'])
public_api_v1_bp.add_route(deposit_money_hdlr, '/account/deposit', methods=['POST'])
public_api_v1_bp.add_route(transfer_money_hdlr, '/transfer', methods=['POST'])
