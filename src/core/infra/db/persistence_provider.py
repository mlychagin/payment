from typing import List

from aiopg.sa import create_engine


class AiopgPersistenceProvider:
    def __init__(
            self,
            user,
            password,
            host,
            port,
            database,
            max_conn=10,
    ) -> None:
        self.engine = create_engine(
            user=user,
            password=password,
            host=host,
            port=port,
            database=database,
            maxsize=max_conn,
            pool_recycle=60,
        )
        import logging
        logging.getLogger('aiopg').setLevel(logging.DEBUG)

    async def persist(self, entity):
        return await entity.orm.save_to_db(self.engine, entity)

    async def connect(self):
        self.engine = await self.engine

    async def close(self):
        self.engine.close()

    async def load_by(self, entity_cls, *args, **kwargs) -> List:
        return await entity_cls.orm.load_from_db_by(
            self.engine,
            *args,
            **kwargs,
        )
