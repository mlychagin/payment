from datetime import datetime
from typing import List

import sqlalchemy as sa
from sqlalchemy import (
    Table,
    Column,
    Integer,
    MetaData,
    DateTime,
    text,
    ForeignKey,
    DECIMAL,
    VARCHAR,
    Boolean,
    and_,
)

from src.core.common.interfaces import DomainModel
from src.core.common.utils import NOT_SET
from src.core.domain.models.transaction import Transaction
from src.core.service_layer.repositories.account import AccountRepo
from src.core.service_layer.repositories.currency import CurrencyRepo
from src.core.service_layer.repositories.user import UserRepo


class BaseModelExc(Exception):
    pass


class ModelNotPersistedExc(BaseModelExc):
    pass


class AccountNotFound(BaseModelExc):
    pass


class BaseTransactionExc(Exception):
    pass


class UnspecifiedTransactionExc(BaseTransactionExc):
    def __init__(self) -> None:
        super().__init__(
            "Transaction exception, possible causes: "
            "1) Withdrawal/insertion account does not exist "
            "2) Withdrawal/insertion currencies don't match "
            "3) Withdrawal sum exceeds balance and overdraft not allowed "
            "4) Insertion is to a tech account "
        )


class WithdrawalExc(UnspecifiedTransactionExc):
    pass


metadata = MetaData()


transaction_history_table = Table(
    "transaction_history",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column(
        "created_at",
        DateTime,
        nullable=False,
        default=datetime.utcnow,
        server_default=text("timezone('utc', now())"),
    ),
    Column("withdrawal_account_id", ForeignKey("account.id", ondelete="RESTRICT"), nullable=False),
    Column("insertion_account_id", ForeignKey("account.id", ondelete="RESTRICT"), nullable=False),
    Column("sum", DECIMAL, nullable=False),
    Column("currency_id", ForeignKey("currency.id", ondelete="RESTRICT"), nullable=False),
    Column("deleted", Boolean, nullable=False),
)

account_table = Table(
    "account",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column(
        "created_at",
        DateTime,
        nullable=False,
        default=datetime.utcnow,
        server_default=text("timezone('utc', now())"),
    ),
    Column("user_id", ForeignKey("user.id", ondelete="RESTRICT"), nullable=True),
    Column("balance", DECIMAL, nullable=False),
    Column("currency_id", ForeignKey("currency.id", ondelete="RESTRICT"), nullable=False),
    Column("is_tech", Boolean, nullable=False),
    Column("deleted", Boolean, nullable=False),
)

currency_table = Table(
    "currency",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("name", VARCHAR, nullable=False),
    Column("deleted", Boolean, nullable=False),
)

user_table = Table(
    "user",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("first_name", VARCHAR, nullable=False),
    Column("last_name", VARCHAR, nullable=False),
    Column("middle_name", VARCHAR, nullable=False),
    Column("deleted", Boolean, nullable=False),
)


class SimpleOrm:
    def __init__(self, table: Table):
        self._tbl = table

    async def save_to_db(self, engine, entity_model_inst):
        async with engine.acquire() as conn:
            model_cls = entity_model_inst.__class__
            model_data = {
                var_name: getattr(entity_model_inst, var_name)
                for var_name in vars(model_cls)
                if isinstance(getattr(model_cls, var_name), property,)
            }
            for k, v in model_data.items():
                if isinstance(v, DomainModel):
                    model_data[k] = v.id
            id = model_data.pop("id")
            if id:
                cur = await conn.execute(
                    sa.update(self._tbl)
                    .where(self._tbl.c.id == id)
                    .values(**model_data)
                    .returning(*self._tbl.c)
                )
            else:
                cur = await conn.execute(
                    sa.insert(self._tbl).values(**model_data).returning(*self._tbl.c)
                )
            return await cur.fetchone()

    async def load_from_db_by(self, engine, ids: List[int] = NOT_SET):
        q = sa.select(self._tbl.c,)
        if ids is not NOT_SET:
            q = q.where(self._tbl.c.id.in_(ids))

        async with engine.acquire() as conn:
            cur = await conn.execute(q)
            return await cur.fetchall()


class TransactionOrm:
    def __init__(self, transaction_history_table: Table, account_table: Table) -> None:
        self.transaction_history_table = transaction_history_table
        self.account_table = account_table

    async def _check_withdrawal_action(self):
        # TODO: проверка состояния объектов в базе и вычисление ошибки
        return WithdrawalExc()

    async def save_to_db(self, engine, model: Transaction):
        async with engine.acquire() as conn:
            async with conn.begin():
                q = (
                    sa.update(self.account_table)
                    .where(
                        and_(
                            self.account_table.c.id == model.withdrawal_account_id,
                            self.account_table.c.currency_id == model.currency_id,
                            self.account_table.c.deleted.is_(False),
                            sa.case(
                                [
                                    (self.account_table.c.is_tech, True),
                                ],
                                else_=self.account_table.c.balance >= model.sum,
                            ),
                        )
                    )
                    .values(balance=self.account_table.c.balance - model.sum)
                    .returning(*self.account_table.c)
                )
                cur = await conn.execute(q)
                if not cur.rowcount:
                    exc = await self._check_withdrawal_action()
                    raise exc
                upd_withdrawal_account = await cur.fetchone()

                cur = await conn.execute(
                    sa.update(self.account_table)
                    .where(
                        and_(
                            self.account_table.c.id == model.insertion_account_id,
                            self.account_table.c.currency_id == model.currency_id,
                            self.account_table.c.deleted.is_(False),
                            self.account_table.c.is_tech.is_(False)
                        )
                    )
                    .values(balance=self.account_table.c.balance + model.sum)
                    .returning(*self.account_table.c)
                )
                if not cur.rowcount:
                    raise UnspecifiedTransactionExc()
                upd_insertion_account = await cur.fetchone()

                cur = await conn.execute(
                    sa.insert(self.transaction_history_table,)
                    .values(
                        withdrawal_account_id=model.withdrawal_account_id,
                        insertion_account_id=model.insertion_account_id,
                        sum=model.sum,
                        currency_id=model.currency_id,
                        deleted=False,
                    )
                    .returning(*self.transaction_history_table.c)
                )
                history_entry = await cur.fetchone()

                return upd_withdrawal_account, upd_insertion_account, history_entry


class TransactionHistoryOrm(SimpleOrm):
    async def load_from_db_by(
        self, engine, ids: List[int] = NOT_SET, withdrawal_account_ids: List[int] = NOT_SET,
    ):
        if ids is not NOT_SET and withdrawal_account_ids is NOT_SET:
            return await super().load_from_db_by(engine, ids)
        q = sa.select(self._tbl.c)
        if ids is not NOT_SET:
            q = q.where(self._tbl.c.id.in_(ids))
        if withdrawal_account_ids is not NOT_SET:
            q = q.where(self._tbl.c.withdrawal_account_id.in_(withdrawal_account_ids))

        async with engine.acquire() as conn:
            cur = await conn.execute(q)
            return await cur.fetchall()


class AccountOrm(SimpleOrm):
    async def load_from_db_by(
        self, engine, ids: List[int] = NOT_SET, is_tech: bool = NOT_SET,
    ):
        if ids is not NOT_SET and is_tech is NOT_SET:
            return await super().load_from_db_by(engine, ids)

        q = sa.select(self._tbl.c)
        if ids is not NOT_SET:
            q = q.where(self._tbl.c.id.in_(ids))
        if is_tech is not NOT_SET:
            q = q.where(self._tbl.c.is_tech.is_(is_tech))

        async with engine.acquire() as conn:
            cur = await conn.execute(q)
            return await cur.fetchall()


class CurrencyOrm(SimpleOrm):
    async def load_from_db_by(self, engine, ids: List[int] = NOT_SET, name: str = NOT_SET):
        if ids is not NOT_SET and name is NOT_SET:
            return await super().load_from_db_by(engine, ids)

        q = sa.select(self._tbl.c)
        if ids is not NOT_SET:
            q = q.where(self._tbl.c.id.in_(ids))
        if name is not NOT_SET:
            q = q.where(self._tbl.c.name == name)

        async with engine.acquire() as conn:
            cur = await conn.execute(q)
            return await cur.fetchall()


def bind_mappers(persistence_provider):
    from src.core.domain.models.account import Account
    from src.core.domain.models.currency import Currency
    from src.core.domain.models.transaction import Transaction
    from src.core.domain.models.user import User
    from src.core.service_layer.repositories.transaction import TransactionRepo
    from src.core.service_layer.repositories.transaction_history import TransactionHistoryRepo
    from src.core.domain.models.transaction_history import TransactionHistory

    repos = {
        "user_repo": UserRepo(persistence_provider=persistence_provider,),
        "account_repo": AccountRepo(persistence_provider=persistence_provider,),
        "currency_repo": CurrencyRepo(persistence_provider=persistence_provider,),
        "transaction_repo": TransactionRepo(persistence_provider=persistence_provider,),
        "transaction_history_repo": TransactionHistoryRepo(
            persistence_provider=persistence_provider,
        ),
    }

    Transaction.orm = TransactionOrm(
        transaction_history_table=transaction_history_table, account_table=account_table,
    )
    User.orm = SimpleOrm(table=user_table,)
    Account.orm = AccountOrm(table=account_table,)
    Currency.orm = CurrencyOrm(table=currency_table,)
    TransactionHistory.orm = TransactionHistoryOrm(table=transaction_history_table)

    return repos
