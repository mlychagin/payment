"""init

Revision ID: 37f26a135ff6
Revises: 
Create Date: 2020-11-22 18:22:08.939911

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '37f26a135ff6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('currency',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('name', sa.VARCHAR(), nullable=False),
        sa.Column('deleted', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('first_name', sa.VARCHAR(), nullable=False),
        sa.Column('last_name', sa.VARCHAR(), nullable=False),
        sa.Column('middle_name', sa.VARCHAR(), nullable=False),
        sa.Column('deleted', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table('account',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('created_at', sa.DateTime(), server_default=sa.text("timezone('utc', now())"), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('balance', sa.DECIMAL(), nullable=False),
        sa.Column('currency_id', sa.Integer(), nullable=False),
        sa.Column('is_tech', sa.Boolean(), nullable=False),
        sa.Column('deleted', sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(['currency_id'], ['currency.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table('transaction_history',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('created_at', sa.DateTime(), server_default=sa.text("timezone('utc', now())"), nullable=False),
        sa.Column('withdrawal_account_id', sa.Integer(), nullable=False),
        sa.Column('insertion_account_id', sa.Integer(), nullable=False),
        sa.Column('sum', sa.DECIMAL(), nullable=False),
        sa.Column('currency_id', sa.Integer(), nullable=False),
        sa.Column('deleted', sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(['currency_id'], ['currency.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['insertion_account_id'], ['account.id'], ondelete='RESTRICT'),
        sa.ForeignKeyConstraint(['withdrawal_account_id'], ['account.id'], ondelete='RESTRICT'),
        sa.PrimaryKeyConstraint('id')
    )
    op.execute("""INSERT INTO currency (name, deleted)
    VALUES
    ('USD', false)
    """)
    op.execute("""INSERT INTO account (balance, currency_id, is_tech, deleted)
    VALUES
    (0, 1, true, false)
    """)


def downgrade():
    op.drop_table('transaction')
    op.drop_table('account')
    op.drop_table('user')
    op.drop_table('currency')
