from decimal import Decimal
from typing import List

from src.core.common.interfaces import RepoABC
from src.core.common.utils import NOT_SET
from src.core.domain.models.account import Account


class BaseAccountException(Exception):
    pass


class BaseWithdrawException(BaseAccountException):
    pass


class AccountRepo(RepoABC):
    def __init__(self, persistence_provider) -> None:
        super().__init__()

        self._persistence_provider = persistence_provider

    @classmethod
    def create_account(cls, user_id, currency_id) -> Account:
        return Account(
            user_id=user_id,
            balance=Decimal('0.0'),
            currency_id=currency_id,
            is_tech=False,
        )

    @classmethod
    def create_tech_account(cls, currency_id) -> Account:
        return Account(
            balance=Decimal('0.0'),
            currency_id=currency_id,
            is_tech=True,
        )

    @classmethod
    def delete_account(cls, account: Account) -> Account:
        account.set_deleted()
        return account

    async def persist(self, account: Account):
        entity = await self._persistence_provider.persist(account)
        return Account(**entity)

    async def load_all(self):
        entities = await self._persistence_provider.load_by(Account)
        return [Account(**i) for i in entities]

    async def get_tech_deposit_account(self):
        accounts = await self.load_by(is_tech=True)
        if not accounts:
            raise Exception('Should be at least one tech account')
        return accounts[0]

    async def load_by(
            self,
            ids: List[int]=NOT_SET,
            is_tech: bool=NOT_SET,
    ):
        entities = await self._persistence_provider.load_by(Account, ids=ids, is_tech=is_tech)
        return [Account(**i) for i in entities]
