from decimal import Decimal
from typing import List

from src.core.common.interfaces import RepoABC
from src.core.common.utils import NOT_SET
from src.core.domain.models.account import Account
from src.core.domain.models.transaction import Transaction
from src.core.domain.models.transaction_history import TransactionHistory


class TransactionRepo(RepoABC):
    def __init__(self, persistence_provider) -> None:
        super().__init__()

        self._persistence_provider = persistence_provider

    @classmethod
    def create_c2c(
            self,
            withdrawal_account_id: int,
            insertion_account_id: int,
            sum: Decimal,
            currency_id: int,
    ) -> Transaction:
        return Transaction(
            withdrawal_account_id=withdrawal_account_id,
            insertion_account_id=insertion_account_id,
            sum=sum,
            currency_id=currency_id,
        )

    async def create_deposit(
            self,
            account_repo,
            insertion_account_id: int,
            sum: Decimal,
            currency_id: int,
    ) -> Transaction:
        withdrawal_account = await account_repo.get_tech_deposit_account()
        return Transaction(
            withdrawal_account_id=withdrawal_account.id,
            insertion_account_id=insertion_account_id,
            sum=sum,
            currency_id=currency_id,
        )

    async def persist(self, transaction: Transaction):
        upd_withdrawal_account, upd_insertion_account, history_entry = (
            await self._persistence_provider.persist(transaction)
        )
        return (
            Account(**upd_withdrawal_account),
            Account(**upd_insertion_account),
            TransactionHistory(**history_entry),
        )

    async def load_by(
            self,
            *,
            ids=NOT_SET,
    ) -> List[Transaction]:
        entities = await self._persistence_provider.load_by(
            Account,
            ids=ids,
        )
        return [Transaction(**i) for i in entities]
