from typing import List

from src.core.common.interfaces import RepoABC
from src.core.domain.models.user import User


class UserRepo(RepoABC):
    def __init__(self, persistence_provider) -> None:
        super().__init__()

        self._persistence_provider = persistence_provider

    @classmethod
    def create_user(
            cls,
            first_name,
            last_name,
            middle_name
    ) -> User:
        return User(
            first_name=first_name,
            last_name=last_name,
            middle_name=middle_name
        )

    @classmethod
    def delete_user(cls, user) -> User:
        user.set_deleted()
        return user

    async def persist(self, user: User) -> User:
        upd_entity = await self._persistence_provider.persist(user)
        return User(**upd_entity)

    async def load_by(
            self,
            *,
            id,
    ) -> List[User]:
        entities = await self._persistence_provider.load_by(
            self.__class__,
            id=id,
        )
        return [User(**i) for i in entities]
