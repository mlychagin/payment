from typing import List, Optional

from src.core.common.interfaces import RepoABC
from src.core.common.utils import NOT_SET
from src.core.domain.models.currency import Currency


class CurrencyRepo(RepoABC):
    def __init__(self, persistence_provider) -> None:
        super().__init__()

        self._persistence_provider = persistence_provider

    @classmethod
    def create(
            self,
            name,
    ) -> Currency:
        return Currency(
            name=name,
            deleted=False,
        )

    async def persist(self, currency: Currency):
        entity = await self._persistence_provider.persist(currency)
        return Currency(**entity)

    async def load_one_by(
            self,
            *,
            id=NOT_SET,
            name=NOT_SET,
    ) -> Optional[Currency]:
        if id is not NOT_SET \
            and name is not NOT_SET:
            raise Exception('Either id or name should be set')
        if id is not NOT_SET:
            params = {'ids': [id,]}
        elif name is not NOT_SET:
            params = {'name': name}
        entities = await self._persistence_provider.load_by(Currency, **params)
        if entities:
            return Currency(**entities[0])
        else:
            return None

