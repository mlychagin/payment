from decimal import Decimal
from typing import List

from src.core.common.interfaces import RepoABC
from src.core.common.utils import NOT_SET
from src.core.domain.models.account import Account
from src.core.domain.models.transaction import Transaction
from src.core.domain.models.transaction_history import TransactionHistory


class TransactionHistoryRepo(RepoABC):
    def __init__(self, persistence_provider) -> None:
        super().__init__()

        self._persistence_provider = persistence_provider

    async def load_by(
            self,
            *,
            withdrawal_account_ids: List[int]=NOT_SET,
    ) -> List[TransactionHistory]:
        entities = await self._persistence_provider.load_by(
            TransactionHistory,
            withdrawal_account_ids=withdrawal_account_ids,
        )
        return [TransactionHistory(**i) for i in entities]

    async def load_all(self):
        return await self.load_by()
