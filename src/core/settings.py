import os

from envparse import Env

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

env = Env()
env.read_envfile(os.path.join(BASE_DIR, ".env"))
env.read_envfile()


TEST_DB_URL = env("TEST_DB_URL", default="postgresql://postgres:postgres@localhost:5422/payment",)
PAYMENT_DB_USER = env("PAYMENT_DB_USER", default="postgres")
PAYMENT_DB_PASSWORD = env("PAYMENT_DB_PASSWORD", default="postgres")
PAYMENT_DB_HOST = env("PAYMENT_DB_HOST", default="localhost")
PAYMENT_DB_PORT = env("PAYMENT_DB_PORT", default="5422")
PAYMENT_DB_NAME = env("PAYMENT_DB_NAME", default="payment")
DB_MAX_CONNECTIONS = env.int("DB_MAX_CONNECTIONS", default=100)

API_SERVER_PORT = env.int('API_SERVER_PORT', default=18080)
