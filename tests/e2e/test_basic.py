from decimal import Decimal


async def test__api_works(app_client, post_clean_tables):
    acc_repo = app_client.app.account_repo
    SUM = Decimal("12.34")

    tech_acc = acc_repo.create_tech_account(
        currency_id=(await app_client.app.get_default_currency()).id
    )
    await acc_repo.persist(tech_acc)

    users = []
    for i in range(2):
        resp = await app_client.post(
            "/api/v1/user",
            json={
                "first_name": f"first_name_{i}",
                "middle_name": f"middle_name_{i}",
                "last_name": f"last_name_{i}",
            },
        )
        user_info = await resp.json()
        assert resp.status == 201, user_info
        users.append(user_info)

    resp = await app_client.post(
        "/api/v1/account/deposit", json={"account_id": users[0]["account_id"], "sum": str(SUM),},
    )
    deposit_result_info = await resp.json()
    assert resp.status == 200, deposit_result_info

    resp = await app_client.post(
        "/api/v1/transfer",
        json={
            "withdrawal_account_id": users[0]["account_id"],
            "insertion_account_id": users[1]["account_id"],
            "sum": str(SUM + 1),
        },
    )
    assert resp.status == 400

    resp = await app_client.post(
        "/api/v1/transfer",
        json={
            "withdrawal_account_id": users[0]["account_id"],
            "insertion_account_id": users[1]["account_id"],
            "sum": str(SUM),
        },
    )
    assert resp.status == 200, await resp.json()
