import pytest
import sqlalchemy as sa


@pytest.yield_fixture(scope='session', autouse=True)
def test_db_engine():
    from src.core.infra.db.orm import metadata
    from src.core import settings
    from sqlalchemy import create_engine
    from sqlalchemy.engine.url import URL
    from sqlalchemy_utils import database_exists, drop_database, create_database

    connection_data = URL(
        drivername='postgresql',
        username=settings.PAYMENT_DB_USER,
        password=settings.PAYMENT_DB_PASSWORD,
        host=settings.PAYMENT_DB_HOST,
        port=settings.PAYMENT_DB_PORT,
        database=settings.PAYMENT_DB_NAME,
    )
    if database_exists(connection_data):
        drop_database(connection_data)
    create_database(connection_data)

    engine = create_engine(connection_data)
    metadata.create_all(engine)

    yield engine

    drop_database(connection_data)


@pytest.yield_fixture
async def app_inst():
    from src.core.infra.app.app_cfg import make_app

    yield make_app()


@pytest.fixture
def app_client(loop, app_inst, sanic_client):
    return loop.run_until_complete(sanic_client(app_inst))


@pytest.yield_fixture
def post_clean_tables(test_db_engine):
    from src.core.infra.db.orm import account_table, user_table, transaction_history_table, currency_table

    yield

    for tbl in (
        transaction_history_table,
        account_table,
        user_table,
        currency_table,
    ):
        test_db_engine.execute(
            sa.delete(tbl).where(True)
        )
