from decimal import Decimal

from pytest import raises

from src.core.infra.app.app_cfg import PaymentApplication
from src.core.infra.db.orm import WithdrawalExc


async def test__cls_work(app_client, post_clean_tables):
    app: PaymentApplication = app_client.app
    user_repo = app.user_repo
    acc_repo = app.account_repo
    currency_repo = app.currency_repo
    transaction_repo = app.transaction_repo
    transaction_history_repo = app.transaction_history_repo

    u = user_repo.create_user(
        'test',
        'test',
        'test',
    )
    await user_repo.persist(u)

    usd = currency_repo.create(name='USD')
    usd = await currency_repo.persist(usd)

    acc1 = acc_repo.create_account(
        u.id,
        usd,
    )
    acc1 = await acc_repo.persist(acc1)
    acc2 = acc_repo.create_account(
        u.id,
        usd,
    )
    acc2 = await acc_repo.persist(acc2)
    tech_acc = acc_repo.create_tech_account(usd)
    tech_acc = await acc_repo.persist(tech_acc)

    SUM = Decimal('12.34')
    t1 = await transaction_repo.create_deposit(
        account_repo=acc_repo,
        insertion_account_id=acc1.id,
        sum=SUM,
        currency_id=usd.id,
    )
    await transaction_repo.persist(t1)

    with raises(WithdrawalExc):
        t2 = transaction_repo.create_c2c(
            withdrawal_account_id=acc1.id,
            insertion_account_id=acc2.id,
            sum=SUM + 1,
            currency_id=usd.id,
        )
        await transaction_repo.persist(t2)

    t2 = transaction_repo.create_c2c(
        withdrawal_account_id=acc1.id,
        insertion_account_id=acc2.id,
        sum=SUM,
        currency_id=usd.id,
    )
    await transaction_repo.persist(t2)

    hist = await transaction_history_repo.load_by(
        withdrawal_account_ids=[tech_acc.id],
    )
    assert all((
        len(hist) == 1,
        hist[0].currency_id == usd.id,
        hist[0].withdrawal_account_id == tech_acc.id,
        hist[0].insertion_account_id == acc1.id,
        hist[0].sum == SUM,
    ))
    hist = await transaction_history_repo.load_by(
        withdrawal_account_ids=[acc1.id],
    )
    assert all((
        len(hist) == 1,
        hist[0].currency_id == usd.id,
        hist[0].withdrawal_account_id == acc1.id,
        hist[0].insertion_account_id == acc2.id,
        hist[0].sum == SUM,
    ))
