FROM python:3.7-alpine

ARG BUILD_PACKAGES="gcc g++ make libc-dev libxslt-dev openssl python3-dev \
                    postgresql-dev linux-headers libffi-dev bash"

RUN apk add --update --no-cache \
    $BUILD_PACKAGES
RUN pip install pipenv

WORKDIR /app

COPY Pipfile Pipfile.lock Makefile /app/
RUN make prod_requirements

ADD . /app

EXPOSE 18080

CMD ["python3", "manage.py", "run-api-server"]
