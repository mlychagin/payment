#!/bin/bash

docker-compose -f docker-compose.test.yml build
docker-compose -f docker-compose.test.yml up --abort-on-container-exit --exit-code-from payment_test
tests_result=$?
docker-compose -f docker-compose.test.yml down --remove-orphans
if [[ ${tests_result} != 0 ]]; then
  echo "tests FAILED"
  exit ${tests_result}
fi
echo "tests OK"
